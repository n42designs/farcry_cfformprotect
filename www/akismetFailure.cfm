
<cfscript>
cffpConfig = application.fc.lib.cffp.getConfig();

if (len(trim(cffpConfig["akismetAPIKeyPrefix"]))) {
	cffpConfig["akismetAPIKey"] = replacenocase(cffpConfig["akismetAPIKey"], cffpConfig["akismetAPIKeyPrefix"], "");
}

</cfscript>

<cfif structKeyExists(url,"type")>
	<cftry>
		<!--- send form contents to Akismet API --->
		<cfhttp url="http://#cffpConfig.akismetAPIKey#.rest.akismet.com/1.1/submit-#url.type#" timeout="10" method="post">
			<cfhttpparam name="key" type="formfield" value="#cffpConfig.akismetAPIKey#">
			<cfhttpparam name="blog" type="formfield" value="#cffpConfig.akismetBlogURL#">
			<cfhttpparam name="user_ip" type="formfield" value="#urlDecode(url.user_ip,'utf-8')#">
			<cfhttpparam name="user_agent" type="formfield" value="#urlDecode(url.user_agent,'utf-8')#">
			<cfhttpparam name="referrer" type="formfield" value="#urlDecode(url.referrer,'utf-8')#">
			<cfif structKeyExists(url, "comment_author")>
			<cfhttpparam name="comment_author" type="formfield" value="#urlDecode(url.comment_author,'utf-8')#">
			</cfif>
			<cfif structKeyExists(url, "comment_author_email")>
			<cfhttpparam name="comment_author_email" type="formfield" value="#urlDecode(url.comment_author_email,'utf-8')#">
			</cfif>
			<cfif structKeyExists(url, "comment_author_url")>
			<cfhttpparam name="comment_author_url" type="formfield" value="#urlDecode(url.comment_author_url,'utf-8')#">
			</cfif>
			<cfif structKeyExists(url, "comment_content")>
			<cfhttpparam name="comment_content" type="formfield" value="#urlDecode(url.comment_content,'utf-8')#">
			</cfif>
			<cfif structKeyExists(url, "comment_date_gmt")>
			<cfhttpparam name="comment_date_gmt" type="formfield" value="#urlDecode(url.comment_date_gmt,'utf-8')#">
			</cfif>
			<!--- <cfhttpparam name="is_test" type="formfield" value="true"> --->
		</cfhttp>
		<cfcatch type="any">
			<cflog application="true" file="cfformprotect" type="error" text="Error reporting SPAM message #cfcatch.message#" />
		</cfcatch>
	</cftry>

	<p>Thank you for reporting the SPAM message</p>

<cfelse>
	<p>Thank you for reporting the SPAM message</p>
</cfif>