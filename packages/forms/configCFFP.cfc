component extends = "farcry.core.packages.forms.forms" key = "cffp" displayname = "CF Form Protect (Front End)" hint = "Manages settings for cfformprotect for use on the front end of the website." {

	property ftSeq = 110 ftFieldset = "Tests" ftLabel = "Mouse Movement" name = "mouseMovement" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 115 ftFieldset = "Tests" ftLabel = "Used Keyboard" name = "usedKeyboard" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 120 ftFieldset = "Tests" ftLabel = "Timed Form" name = "timedFormSubmission" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 125 ftFieldset = "Tests" ftLabel = "Hidden Field" name = "hiddenFormField" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 130 ftFieldset = "Tests" ftLabel = "Too Many URLs" name = "tooManyUrls" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 135 ftFieldset = "Tests" ftLabel = "Test Strings" name = "teststrings" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;

	property ftSeq = 210 ftFieldset = "Settings" ftLabel = "Timed Form Min Seconds" name = "timedFormMinSeconds" type = "numeric" ftType = "integer" default = 5 ftDefault = 5 required = true;
	property ftSeq = 215 ftFieldset = "Settings" ftLabel = "Timed Form Max Seconds" name = "timedFormMaxSeconds" type = "numeric" ftType = "integer" default = 3600 ftDefault = 3600 required = true;
	property ftSeq = 220 ftFieldset = "Settings" ftLabel = "Encryption Key" name = "encryptionKey" type = "nstring" ftType = "string" default = "CfF0rmProt3ct_F@rCry" ftDefault = "CfF0rmProt3ct_F@rCry" required = true;
	property ftSeq = 225 ftFieldset = "Settings" ftLabel = "Too Many URLs Max URLs" name = "tooManyUrlsMaxUrls" type = "numeric" ftType = "integer" default = 6 ftDefault = 6 required = true;
	property ftSeq = 230 ftFieldset = "Settings" ftLabel = "Spam Strings" name = "spamstrings" type = "longchar" ftType = "longchar" default = "free music,download music,music downloads,viagra,phentermine,viagra,tramadol,ultram,prescription soma,cheap soma,cialis,levitra,weight loss,buy cheap" ftDefault = "free music,download music,music downloads,viagra,phentermine,viagra,tramadol,ultram,prescription soma,cheap soma,cialis,levitra,weight loss,buy cheap" required = false;

	property ftSeq = 310 ftFieldset = "Akismet" ftLabel = "Akismet" name = "akismet" type = "boolean" ftType = "boolean" default = 0 ftDefault = 0 required = true;
	property ftSeq = 315 ftFieldset = "Akismet" ftLabel = "API Key" name = "akismetAPIKey" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;
	property ftSeq = 316 ftFieldset = "Akismet" ftLabel = "API Key Prefix" name = "akismetAPIKeyPrefix" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false ftHint = "Akismet will sometimes give you a key that CF can consider to be a number in scientific notation. EX: 12345678e987 This will break this plugin.  To combat this, when you enter your key, give it a alpha prefix 'MYSITE_' for example so you'd enter MYSITE_12345678e987 as the key.  Then enter the prefix in this field.  This will allow us to store the key in a way that CF will never consider it a numeric value, and we can remove the prefix before we send it to Akismet.";
	property ftSeq = 320 ftFieldset = "Akismet" ftLabel = "Blog URL" name = "akismetBlogURL" type = "nstring" ftType = "string" default = "" ftDefault = "'https://' & cgi.server_name & '/'" ftDefaultType = "evaluate" required = false;

	property ftSeq = 410 ftFieldset = "Project Honey Pot" ftLabel = "Project Honey Pot" name = "projectHoneyPot" type = "boolean" ftType = "boolean" default = 0 ftDefault = 0 required = false;
	property ftSeq = 415 ftFieldset = "Project Honey Pot" ftLabel = "API Key" name = "projectHoneyPotAPIKey" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;

	property ftSeq = 510 ftFieldset = "Points" ftLabel = "Mouse Movement" name = "mouseMovementPoints" type = "numeric" ftType = "integer" default = 1 ftDefault = 1 required = true;
	property ftSeq = 515 ftFieldset = "Points" ftLabel = "Used Keyboard" name = "usedKeyboardPoints" type = "numeric" ftType = "integer" default = 1 ftDefault = 1 required = true;
	property ftSeq = 520 ftFieldset = "Points" ftLabel = "Timed Form" name = "timedFormPoints" type = "numeric" ftType = "integer" default = 2 ftDefault = 2 required = true;
	property ftSeq = 525 ftFieldset = "Points" ftLabel = "Hidden Field" name = "hiddenFieldPoints" type = "numeric" ftType = "integer" default = 3 ftDefault = 3 required = true;
	property ftSeq = 535 ftFieldset = "Points" ftLabel = "Too Many URLs" name = "tooManyUrlsPoints" type = "numeric" ftType = "integer" default = 3 ftDefault = 3 required = true;
	property ftSeq = 540 ftFieldset = "Points" ftLabel = "SPAM Strings" name = "spamStringPoints" type = "numeric" ftType = "integer" default = 2 ftDefault = 2 required = true;
	property ftSeq = 545 ftFieldset = "Points" ftLabel = "Project Honey Pot" name = "projectHoneyPotPoints" type = "numeric" ftType = "integer" default = 3 ftDefault = 3 required = true;
	property ftSeq = 550 ftFieldset = "Points" ftLabel = "Akismet" name = "akismetPoints" type = "numeric" ftType = "integer" default = 3 ftDefault = 3 required = true;
	property ftSeq = 555 ftFieldset = "Points" ftLabel = "Failure Limit" name = "failureLimit" type = "numeric" ftType = "integer" default = 3 ftDefault = 3 required = true ftHint = "how many points will flag the form submission as spam";

	property ftSeq = 610 ftFieldset = "Email" ftLabel = "Email Failed Tests?" name = "emailFailedTests" type = "boolean" ftType = "boolean" default = 0 ftDefault = 0 required = true;
	property ftSeq = 615 ftFieldset = "Email" ftLabel = "Server" name = "emailServer" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;
	property ftSeq = 620 ftFieldset = "Email" ftLabel = "Username" name = "emailUserName" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;
	property ftSeq = 625 ftFieldset = "Email" ftLabel = "Password" name = "emailPassword" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;
	property ftSeq = 630 ftFieldset = "Email" ftLabel = "From" name = "emailFromAddress" type = "nstring" ftType = "email" default = "" ftDefault = "" required = false;
	property ftSeq = 635 ftFieldset = "Email" ftLabel = "To" name = "emailToAddress" type = "nstring" ftType = "email" default = "" ftDefault = "" required = false;
	property ftSeq = 640 ftFieldset = "Email" ftLabel = "Subject" name = "emailSubject" type = "nstring" ftType = "string" default = "" ftDefault = "" required = false;

	property ftSeq = 710 ftFieldset = "Logging" ftLabel = "Log Failed Tests?" name = "logFailedTests" type = "boolean" ftType = "boolean" default = 1 ftDefault = 1 required = true;
	property ftSeq = 715 ftFieldset = "Logging" ftLabel = "Log File" name = "logFile" type = "nstring" ftType = "string" default = "cfformprotect" ftDefault = "cfformprotect" required = true;

}