component {

	public struct function getConfig() {
		return {
			"mouseMovement" = application.fapi.getConfig(key = "cffp", name = "mouseMovement"),
			"usedKeyboard" = application.fapi.getConfig(key = "cffp", name = "usedKeyboard"),
			"timedFormSubmission" = application.fapi.getConfig(key = "cffp", name = "timedFormSubmission"),
			"hiddenFormField" = application.fapi.getConfig(key = "cffp", name = "hiddenFormField"),
			"akismet" = application.fapi.getConfig(key = "cffp", name = "akismet"),
			"tooManyUrls" = application.fapi.getConfig(key = "cffp", name = "tooManyUrls"),
			"teststrings" = application.fapi.getConfig(key = "cffp", name = "teststrings"),
			"projectHoneyPot" = application.fapi.getConfig(key = "cffp", name = "projectHoneyPot"),
			"timedFormMinSeconds" = application.fapi.getConfig(key = "cffp", name = "timedFormMinSeconds"),
			"timedFormMaxSeconds" = application.fapi.getConfig(key = "cffp", name = "timedFormMaxSeconds"),
			"encryptionKey" = application.fapi.getConfig(key = "cffp", name = "encryptionKey"),
			"akismetAPIKey" = application.fapi.getConfig(key = "cffp", name = "akismetAPIKey"),
			"akismetAPIKeyPrefix" = application.fapi.getConfig(key = "cffp", name = "akismetAPIKeyPrefix"),
			"akismetBlogURL" = application.fapi.getConfig(key = "cffp", name = "akismetBlogURL"),
			"tooManyUrlsMaxUrls" = application.fapi.getConfig(key = "cffp", name = "tooManyUrlsMaxUrls"),
			"spamstrings" = application.fapi.getConfig(key = "cffp", name = "spamstrings"),
			"projectHoneyPotAPIKey" = application.fapi.getConfig(key = "cffp", name = "projectHoneyPotAPIKey"),
			"mouseMovementPoints" = application.fapi.getConfig(key = "cffp", name = "mouseMovementPoints"),
			"usedKeyboardPoints" = application.fapi.getConfig(key = "cffp", name = "usedKeyboardPoints"),
			"timedFormPoints" = application.fapi.getConfig(key = "cffp", name = "timedFormPoints"),
			"hiddenFieldPoints" = application.fapi.getConfig(key = "cffp", name = "hiddenFieldPoints"),
			"akismetPoints" = application.fapi.getConfig(key = "cffp", name = "akismetPoints"),
			"tooManyUrlsPoints" = application.fapi.getConfig(key = "cffp", name = "tooManyUrlsPoints"),
			"spamStringPoints" = application.fapi.getConfig(key = "cffp", name = "spamStringPoints"),
			"projectHoneyPotPoints" = application.fapi.getConfig(key = "cffp", name = "projectHoneyPotPoints"),
			"failureLimit" = application.fapi.getConfig(key = "cffp", name = "failureLimit"),
			"emailFailedTests" = application.fapi.getConfig(key = "cffp", name = "emailFailedTests"),
			"emailServer" = application.fapi.getConfig(key = "cffp", name = "emailServer"),
			"emailUserName" = application.fapi.getConfig(key = "cffp", name = "emailUserName"),
			"emailPassword" = application.fapi.getConfig(key = "cffp", name = "emailPassword"),
			"emailFromAddress" = application.fapi.getConfig(key = "cffp", name = "emailFromAddress"),
			"emailToAddress" = application.fapi.getConfig(key = "cffp", name = "emailToAddress"),
			"emailSubject" = application.fapi.getConfig(key = "cffp", name = "emailSubject"),
			"logFailedTests" = application.fapi.getConfig(key = "cffp", name = "logFailedTests"),
			"logFile" = application.fapi.getConfig(key = "cffp", name = "logFile")
		};
	}

	public boolean function verify(required struct formScope, string akismetCommentType = "comment", string akismetFormNameField, string akismetFormEmailField, string akismetFormURLField, string akismetFormBodyField, array addtlAkismetParams = []) {

		var o = new farcry.plugins.cfformprotect.packages.custom.cffpVerify();

		var args = {
			FormStruct = arguments.formScope,
			akismetCommentType = arguments.akismetCommentType
		};

		if (structKeyExists(arguments, "akismetFormNameField")) {
			args.akismetFormNameField = arguments.akismetFormNameField;
		}
		if (structKeyExists(arguments, "akismetFormEmailField")) {
			args.akismetFormEmailField = arguments.akismetFormEmailField;
		}
		if (structKeyExists(arguments, "akismetFormURLField")) {
			args.akismetFormURLField = arguments.akismetFormURLField;
		}
		if (structKeyExists(arguments, "akismetFormBodyField")) {
			args.akismetFormBodyField = arguments.akismetFormBodyField;
		}

		var result = o.testSubmission(argumentCollection = args);

		return result;

	}

}